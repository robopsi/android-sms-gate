package com.github.axet.smsgate.dialogs;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.SwitchCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.SuperUser;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.MainApplication;
import com.github.axet.smsgate.app.ScheduleTime;
import com.github.axet.smsgate.services.CommandsService;
import com.github.axet.androidlibrary.services.DeviceAdmin;

import java.util.Calendar;

public class RebootDialogFragment extends DialogFragment {
    View v;

    SwitchCompat sw;
    Spinner repeat;
    TextView date;
    TextView time;
    TextView next;

    ScheduleTime schedule;

    public static ScheduleTime getSchedule(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String s = prefs.getString(MainApplication.PREF_REBOOT, "");
        if (s.isEmpty()) {
            return new ScheduleTime(context);
        } else {
            return new ScheduleTime(context, s);
        }
    }

    public static void saveSchedule(Context context, ScheduleTime s) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(MainApplication.PREF_REBOOT, s.save().toString());
        edit.commit();
    }

    public static void schedule(Context context) {
        ScheduleTime s = getSchedule(context);
        s.fired();
        saveSchedule(context, s);

        Intent intent = (new Intent(context, CommandsService.class));
        intent.setAction(CommandsService.REBOOT);
        if (s.enabled) {
            AlarmManager.setExact(context, s.next, intent);
        } else {
            AlarmManager.cancel(context, intent);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener)
            ((DialogInterface.OnDismissListener) a).onDismiss(dialog);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        LinearLayout title = (LinearLayout) inflater.inflate(R.layout.reboot_title, null, false);
        TextView text = (TextView) title.findViewById(R.id.item_text);
        text.setText("Schedule Reboot");
        sw = (SwitchCompat) title.findViewById(R.id.item_switch);
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity())
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                save();
                                dialog.dismiss();
                                onDismiss(dialog);
                            }
                        }
                )
                .setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                onDismiss(dialog);
                            }
                        }
                )
                .setNeutralButton("Reboot",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        }
                )
                .setCustomTitle(title)
                .setView(createView(LayoutInflater.from(getContext()), null, savedInstanceState));
        final AlertDialog d = b.create();
        d.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = d.getButton(DialogInterface.BUTTON_NEUTRAL);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SuperUser.rootTest();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Reboot Device");
                        builder.setMessage("Are you sure?");
                        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DeviceAdmin.reboot(getContext());
                            }
                        });
                        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        builder.show();
                    }
                });
            }
        });
        return d;
    }

    @Nullable
    @Override
    public View getView() {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return null;
    }

    public View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.reboot_edit, container, false);

        schedule = getSchedule(getContext());

        sw.setChecked(schedule.enabled);
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                schedule.enabled = isChecked;
            }
        });

        final Calendar start = Calendar.getInstance();
        start.setTimeInMillis(schedule.start);
        start.set(Calendar.HOUR_OF_DAY, schedule.hour);
        start.set(Calendar.MINUTE, schedule.min);

        next = (TextView) v.findViewById(R.id.schedule_edit_next);

        date = (TextView) v.findViewById(R.id.schedule_edit_date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        start.set(Calendar.YEAR, year);
                        start.set(Calendar.MONTH, monthOfYear);
                        start.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        schedule.setTime(start.getTimeInMillis());
                        updateDate();
                    }
                }, start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        time = (TextView) v.findViewById(R.id.schedule_edit_time);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog dialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        start.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        start.set(Calendar.MINUTE, minute);
                        schedule.setTime(start.getTimeInMillis());
                        updateDate();
                    }
                }, start.get(Calendar.HOUR_OF_DAY), start.get(Calendar.MINUTE), DateFormat.is24HourFormat(getActivity()));
                dialog.show();
            }
        });

        repeat = (Spinner) v.findViewById(R.id.schedule_edit_repeat);
        ArrayAdapter a = ScheduleTime.create(getContext());
        a.remove(a.getItem(0));
        repeat.setAdapter(a);
        repeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                schedule.repeat = ((ScheduleTime.SpinnerItem) repeat.getAdapter().getItem(position)).id;
                schedule.setTime(start.getTimeInMillis());
                updateDate();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        update();

        return v;
    }

    void update() {
        final Calendar start = Calendar.getInstance();
        start.setTimeInMillis(schedule.start);

        repeat.setSelection(ScheduleTime.find(repeat.getAdapter(), schedule.repeat));

        updateDate();
    }

    void updateDate() {
        date.setText(schedule.formatDate());
        time.setText(schedule.formatTime());
        if (schedule.next != 0) {
            next.setText("next: " + schedule.formatDate(schedule.next) + " " + schedule.formatTime(getActivity(), schedule.next));
            next.setVisibility(View.VISIBLE);
        } else {
            next.setVisibility(View.GONE);
        }
    }

    void save() {
        // date/time already saved
        saveSchedule(getContext(), schedule);
        RebootDialogFragment.schedule(getContext());
    }
}
