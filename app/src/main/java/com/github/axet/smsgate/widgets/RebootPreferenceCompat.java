package com.github.axet.smsgate.widgets;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.util.AttributeSet;

import com.github.axet.androidlibrary.app.SuperUser;
import com.github.axet.smsgate.app.ScheduleTime;
import com.github.axet.smsgate.dialogs.RebootDialogFragment;

public class RebootPreferenceCompat extends CheckBoxPreference {

    AppCompatActivity a;
    Fragment f;

    @TargetApi(21)
    public RebootPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        create();
    }

    @TargetApi(21)
    public RebootPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    public RebootPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public RebootPreferenceCompat(Context context) {
        super(context);
        create();
    }

    void create() {
        onResume();
    }

    public void setActivity(AppCompatActivity a) {
        this.a = a;
    }

    public void setFragment(Fragment f) {
        this.f = f;
    }

    public void onResume() {
        updateReboot();
        setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Context context = getContext();
                if (Build.VERSION.SDK_INT >= 24) {
                    boolean b = (boolean) o;
                    if (b) {
                        DevicePolicyManager dpm = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                        if (!dpm.isDeviceOwnerApp(context.getPackageName()) && !SuperUser.isReboot()) {
                            Dialog d = new AlertDialog.Builder(context)
                                    .setTitle("Enable Device Owner")
                                    .setMessage("Device owner has to be enabled manually or device must be rooted")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                        }
                                    })
                                    .create();
                            d.show();
                            return false;
                        }
                    }
                } else {
                    if (!SuperUser.isReboot()) {
                        Dialog d = new AlertDialog.Builder(context)
                                .setTitle("Enable Reboot")
                                .setMessage("Device must be rooted")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                    }
                                })
                                .create();
                        d.show();
                        return false;
                    }
                }
                rebootDialog();
                return false;
            }
        });
    }

    public void rebootDialog() {
        FragmentManager fm = null;
        if (a != null)
            fm = a.getSupportFragmentManager();
        if (f != null)
            fm = f.getActivity().getSupportFragmentManager();
        RebootDialogFragment dialog = new RebootDialogFragment();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        dialog.show(fm, "");
    }

    public void updateReboot() {
        ScheduleTime schedule = RebootDialogFragment.getSchedule(getContext());
        setChecked(schedule.enabled);

        String summary;

        if (isChecked()) {
            summary = schedule.formatStatus();
        } else {
            summary = "Schedule phone restarts";
            if (Build.VERSION.SDK_INT < 24 && !SuperUser.isRooted())
                summary += " (rooted phones only)";
        }

        setSummary(summary);
    }

}
