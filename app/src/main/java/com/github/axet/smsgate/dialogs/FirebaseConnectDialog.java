package com.github.axet.smsgate.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.axet.androidlibrary.crypto.Bitcoin;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.androidlibrary.widgets.PopupShareActionProvider;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.MainApplication;
import com.github.axet.smsgate.services.FirebaseService;

public class FirebaseConnectDialog extends AlertDialog {
    private Button mConnectButton;

    private ImageView mStatusIcon;
    private ImageView refresh;
    private ImageView edit;
    private ImageView mShareIcon;

    private TextView mStatusLabel;
    private TextView mSyncDetailsLabel;

    public FirebaseConnectDialog(final Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        View firebase = inflater.inflate(R.layout.firebase_connect, null, false);

        mConnectButton = (Button) firebase.findViewById(R.id.connect_button);
        mConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(getContext());
                boolean c = prefs.getBoolean(MainApplication.PREF_FIREBASE, false);
                if (c) {
                    FirebaseService.stop(getContext());
                    FirebaseService.reset(getContext());
                } else {
                    FirebaseService.start(getContext());
                }
            }
        });

        setTitle("Firebase Connect");

        edit = (ImageView) firebase.findViewById(R.id.edit);
        refresh = (ImageView) firebase.findViewById(R.id.refresh);
        mStatusIcon = (ImageView) firebase.findViewById(R.id.status_icon);
        mShareIcon = (ImageView) firebase.findViewById(R.id.share_icon);
        mStatusLabel = (TextView) firebase.findViewById(R.id.status_label);
        mSyncDetailsLabel = (TextView) firebase.findViewById(R.id.details_sync_label);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.getApp(getContext()).getKeyPair().generate();
                MainApplication.getApp(getContext()).save();
                FirebaseService.reset(getContext());
                update();
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final OpenFileDialog.EditTextDialog editTextDialog = new OpenFileDialog.EditTextDialog(getContext());
                editTextDialog.setTitle("PrivateKey");
                editTextDialog.setText(MainApplication.getApp(getContext()).getKeyPair().getSec());
                editTextDialog.setPositiveButton("OK", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bitcoin k = MainApplication.getApp(getContext()).getKeyPair();
                        k.loadSec(editTextDialog.getText());
                        k.loadPub(k.sec);
                        MainApplication.getApp(getContext()).save();
                        update();
                    }
                });
                editTextDialog.setNegativeButton("Cancel", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                editTextDialog.show();
            }
        });

        mShareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupShareActionProvider popup = new PopupShareActionProvider(getContext(), mShareIcon);

                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, "");
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "SMSGate PrivateKey");
                emailIntent.putExtra(Intent.EXTRA_TEXT, MainApplication.getApp(getContext()).getKeyPair().getSec());

                popup.setShareIntent(emailIntent);
                popup.show();
            }
        });

        setTitle("Firebase Connect");

        setContentView(firebase);

        update();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void update() {
        mSyncDetailsLabel.setText(MainApplication.getApp(getContext()).getKeyPair().getSec());

        SharedPreferences prefs = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean connected = prefs.getBoolean(MainApplication.PREF_FIREBASE, false);

        if (connected) {
            mConnectButton.setText("Disconnect");
            if (FirebaseService.connected()) {
                mStatusLabel.setText("Connected");
                mStatusIcon.setImageResource(R.drawable.ic_done);
            } else {
                mStatusIcon.setImageResource(R.drawable.ic_idle);
                if (FirebaseService.isPause(getContext()))
                    mStatusLabel.setText("Paused (Wifi Only)");
                else
                    mStatusLabel.setText("Connecting...");
            }
        } else {
            mStatusLabel.setText("Disconnected");
            mStatusIcon.setImageResource(R.drawable.ic_error);
            mConnectButton.setText("Connect");
        }
    }
}
