function int(num) {
  return Math.floor(num);
}
module.exports.int = int;

function pad(num, size) {
    var s = num + "";
    while (s.length < size)
      s = "0" + s;
    return s;
}
module.exports.pad = pad;

module.exports.formatDate = function(t) {
  return t.getFullYear() + '/' + pad(t.getMonth(), 2) + '/' + pad(t.getDate(), 2);
}

module.exports.formatTime = function(t) {
  return pad(t.getHours(), 2) + ':' + pad(t.getMinutes(), 2) + ':' + pad(t.getSeconds(), 2);
}

module.exports.formatDuration = function(diff) {
  var diffMilliseconds = (diff % 1000);
  var diffSeconds = int(diff / 1000 % 60);
  var diffMinutes = int(diff / (60 * 1000) % 60);
  var diffHours = int(diff / (60 * 60 * 1000) % 24);
  var diffDays = int(diff / (24 * 60 * 60 * 1000));
   
  var time = '';
  if (diffDays > 0 ) {
    time += diffDays + " days ";
    time += pad(diffHours, 2) + ":" + pad(diffMinutes, 2) + ":" + pad(diffSeconds, 2);
  }else if (diffHours > 0 ) {
    time += diffHours + ":" + pad(diffMinutes, 2) + ":" + pad(diffSeconds, 2);
  }else if (diffMinutes > 0 ) {
    time += diffMinutes + ":" + pad(diffSeconds, 2);
  }else if (diffSeconds > 0 ) {
    time += diffSeconds + " seconds ";
  }
  return time;
}

module.exports.formatSize = function(s) {
  if (s > 0.1 * 1024 * 1024 * 1024) {
    f = s / 1024 / 1024 / 1024;
    return f.toFixed(1) + " GB";
  } else if (s > 0.1 * 1024 * 1024) {
    f = s / 1024 / 1024;
    return f.toFixed(1) + " MB";
  } else {
    f = s / 1024;
    return f.toFixed(1) + " kb";
  }
}
